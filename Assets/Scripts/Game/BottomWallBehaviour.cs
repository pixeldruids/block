﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
public class BottomWallBehaviour : MonoBehaviour {

	public Vector3 m_StartPosition;
	bool m_Visible = true;
	public bool m_AutoPlay = false;
	public float m_VisibleTime = 0.5f;
	public GameController m_GameController;
	
	void Start()
	{
		m_GameController = GameObject.Find ("Game Controller").GetComponent<GameController> ();
		m_StartPosition = this.transform.position;
		HideBottom();
	}
	
	// Update is called once per frame
	void Update () {
		EventSystem eventSystem = EventSystem.current;
		if (eventSystem.currentSelectedGameObject != null) 
			return;
		bool touchHandled = false;
		for (var i = 0; i < Input.touchCount; ++i) 
		{        
			if (Input.GetTouch(i).phase == TouchPhase.Began)     
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit, 100)) {
					if (hit.collider.name == "inputCatch")
					{
						HandleInput();
						touchHandled = true;
						m_GameController.debug_text.text = "Touch Input";
					}
				}		
			}
		}
		if (Input.GetMouseButtonDown (0) && !touchHandled)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100)) {
				if (hit.collider.name == "inputCatch")
				{
					HandleInput();
					m_GameController.debug_text.text = "Mouse Input";
				}
			}		
			
		}
		
	}
	void HandleInput()
	{
		if (m_GameController.m_GameState != GameController.GameState.Init)
			HandleClick();
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.collider.CompareTag ("ball")) 
		{
		// for each of balls contacts
			foreach (ContactPoint contact in other.contacts) {
				m_GameController.IncScore();
				BallScript ball = other.transform.root.gameObject.GetComponent<BallScript>();
				ball.UpdateBallParams(ball.m_LastContactPoint.x - contact.point.x);
				break;
		}
		}
	}

	public void HandleClick()
	{
		if (m_AutoPlay)
			return;
		if (!m_Visible)
		{
			ShowBottom();
			Invoke ("HideBottom", m_VisibleTime);
		}
	}

	public void ShowBottom()
	{
		transform.position = m_StartPosition;
		m_Visible = true;
	}
	public void HideBottom()
	{
		m_Visible = false;
		transform.position = m_StartPosition + new Vector3 (0, 50.5f, 0);

	}

}
