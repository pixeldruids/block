﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleManager : MonoBehaviour {

	public ObstacleBehaviour []ObstacleList;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("SpawnObstacle", 30.0f, 15.0f);
	}

	void SpawnObstacle()
	{
		List<int> active_list = new List<int>();
		for (int i = 0; i < ObstacleList.Length; i++) {
			if (ObstacleList[i].CanSpawn())
				active_list.Add(i);
		}
		if (active_list.Count > 0) {
			ObstacleList[active_list[Random.Range(0, active_list.Count)]].Spawn( Random.Range(10, 15));
		}
	}
}
