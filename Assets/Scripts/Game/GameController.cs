﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonsterLove.Collections;

public class GameController : MonoBehaviour {

	public enum GameState
	{
		Init,
		Running,
		GameOver
	}
	public bool paused = false;
	int m_countdownTimer = 3;
	public int m_Score = 100;
	public static int AllBallSpeed = 10;
	public GameState m_GameState = GameState.Init;
	public GameObject m_Ball;
	public GameObject m_CDText;
	public AudioClip m_BallHitSfx;
	public AudioSource m_SoundManager;
	public InGameGUI m_GUIManager;
	public AutoPlayBehaviour m_AutoPlayManager;
	public Material[] m_BallMaterialList;
	public Color[] m_ColorList;

	public TextMesh debug_text;

	void Awake() 
	{
	}
	public Material GetBallMaterial(int index)
	{
		return m_BallMaterialList [index];
	}
	public Color GetRandomColor()
	{
		return m_ColorList[Random.Range(0, m_ColorList.Length)];
	}
	public Color GetParticleStColor(int index)
	{
		return m_ColorList [index];
	}
	// Use this for initialization
	void Start () 
	{
		m_SoundManager = GetComponentInChildren<AudioSource> ();
		PoolManager.WarmPool (m_Ball, 5);
		LaunchGame ();
	}
	public int GetScore()
	{
		return m_Score;
	}
	public void IncScore()
	{
		m_Score += PoolManager.GetActiveCount(m_Ball);
		m_GUIManager.m_scoreText.text = m_Score.ToString ();
		PlayBallHitSfx ();
	}

	public void PlayBallHitSfx()
	{
		m_SoundManager.PlayOneShot(m_BallHitSfx);
	}

	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Escape) && m_GameState != GameState.GameOver)
		{
			DePause();
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			AddBall();
		}

	}

	public void LaunchGame()
	{
		m_countdownTimer = 3;
		m_Score = 0;
		m_GameState = GameState.Init;
		m_GUIManager.m_CountdownText = Instantiate (m_CDText).GetComponent<TextMesh>();
		Invoke("updateCountdownText", 1.0f);
	}

	void updateCountdownText()
	{

		m_countdownTimer--;
		m_GUIManager.m_CountdownText.text = m_countdownTimer.ToString ();
		if (m_countdownTimer > 0)
			Invoke ("updateCountdownText", 1.0f);
		else 
		{
			m_GUIManager.m_CountdownText.text = "";
			AddBall();
			m_GameState = GameState.Running;
		}
	}
	public  void Reset()
	{
		LaunchGame ();
	}

	//-----------------------------------------------------
	// Enable / Disablep pause menu when esc is hit
	public void DePause()
	{
		paused = !paused;
		Time.timeScale = paused ?  0 : 1;
	}
	
	//-----------------------------------------------------
	// Unpause only (when changing scenes)
	public void DisablePause()
	{
		paused = false;
		Time.timeScale = 1;
	}

	public void CheckEndConition()
	{
		if (PoolManager.GetActiveCount (m_Ball) == 0)
			Lose ();
	}
	//-----------------------------------------------------
	// load lose screen
	public void Lose()
	{
		m_GameState = GameState.GameOver;
		Application.LoadLevel("losescreen");
	}

	public void AddBall()
	{
		BallScript m_BallScript = PoolManager.SpawnObject(m_Ball, Vector3.zero, Quaternion.identity).GetComponent<BallScript>();
		m_BallScript.Launch(Random.Range(0, m_BallMaterialList.Length));
	}
	public void SlowdownBalls()
	{
		List<GameObject> ball_pool = PoolManager.GetPool (m_Ball).GetUsedItemList(m_Ball);
		foreach(var ball in ball_pool)
		{
			BallScript bs = ball.GetComponent<BallScript>();
			if(bs != null)
			{
				bs.SlowDown();
			}
		}
	}
	
	public void AutoPlay()
	{
		m_AutoPlayManager.StartTimer (15);
	}
}
