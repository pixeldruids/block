﻿using UnityEngine;
using System.Collections;

public class InGameGUI : MonoBehaviour {
	public TextMesh m_scoreText;
	public TextMesh m_CountdownText;
	public TextMesh m_ActiveBallsText;
	public GameObject m_Ball;

	// Use this for initialization
	void Start () {	
	}
	
	// Update is called once per frame
	void Update () {
		m_ActiveBallsText.text = PoolManager.GetActiveCount (m_Ball).ToString();
	}
}
