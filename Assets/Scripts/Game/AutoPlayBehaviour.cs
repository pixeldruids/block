using UnityEngine;
using System.Collections;

public class AutoPlayBehaviour : MonoBehaviour {

	public BottomWallBehaviour bottomPaddle;
	public TextMesh m_TimerText;
	int m_RemainingTimer = 0;
	void Start()
	{
		m_TimerText.text = "";
	}
	public void StartTimer(int extra_time)
	{
		m_RemainingTimer += extra_time;
		CancelInvoke ("Tick");
		Tick ();
		bottomPaddle.m_AutoPlay = true;
		bottomPaddle.ShowBottom ();
	}
	void Tick()
	{
		--m_RemainingTimer;
		m_TimerText.text = m_RemainingTimer.ToString ();
		if (m_RemainingTimer <= 0) 
		{
			CancelInvoke ("Tick");
			m_RemainingTimer = 0;

			bottomPaddle.m_AutoPlay = false;
			bottomPaddle.HideBottom();
			bottomPaddle.HandleClick();
		} 
		else
		{
			Invoke ("Tick", 1.0f);
		}
	}
	void OnTriggerEnter(Collider collider)
	{
	//	bottomPaddle.HandleClick ();
	}
}
