﻿using UnityEngine;
using System.Collections;

public class ObstacleBehaviour : MonoBehaviour {

	public int m_lifeTime = 5;
	GameController m_GameController;
	bool m_Active = false;
	Vector3 m_startPosition;
	// Use this for initialization
	void Start () {
		m_startPosition = transform.position;
		DeSpawn ();
	}

	public void Spawn(int life_time)
	{
		m_lifeTime = life_time;
		transform.position = m_startPosition;
		LeanTween.rotateAround( gameObject, Vector3.forward, 360f, 30f).setRepeat(-1).setEase(LeanTweenType.easeSpring);
		m_GameController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController>();
		Renderer rend = GetComponent<Renderer>();
		rend.material.SetColor("_Color", m_GameController.GetRandomColor ());
		m_Active = true;
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.collider.CompareTag ("ball")) 
		{
			if (m_lifeTime <= 0)
			{
				DeSpawn();
				return;
			}
			m_lifeTime--;
			m_GameController.IncScore();
		}
	}
	public void DeSpawn()
	{
		transform.position = m_startPosition +new  Vector3(0, 1000, 0);
		m_Active = false;
	}
	public bool CanSpawn() { return !m_Active;}
}
