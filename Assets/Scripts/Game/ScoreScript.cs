﻿using UnityEngine;
using System.Collections;

public class ScoreScript : MonoBehaviour {
	public GameController m_GameController;
	void Start()
	{
		m_GameController = GameObject.Find ("Game Controller").GetComponent<GameController> ();
	}
	// Update is called once per frame
	void Update () 
	{
		GetComponent<GUIText>().text = "SCORE: " + m_GameController.GetScore();
	}
}
