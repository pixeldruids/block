﻿using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour {


	public GameObject m_BallObject;
	public GameObject m_TextureObject;
	public Vector3 m_LastContactPoint;
	public GameObject m_TrailParticle;
	float m_Step = 1.0f;
	float m_MaxYVelocity = 40;
	float m_YVelocity = 10;
	Color m_Color;

	Rigidbody m_RigidBody;
	public GameController m_GameController;

	void SetBallType(int type)
	{
		MeshRenderer mesh = m_TextureObject.GetComponent<MeshRenderer> ();
		mesh.material = m_GameController.GetBallMaterial (type);
		m_Color = m_GameController.GetParticleStColor (type);
	}
	public void Launch(int type)
	{
		m_GameController = GameObject.Find ("Game Controller").GetComponent<GameController> ();
		m_RigidBody = GetComponent<Rigidbody> ();
		SetBallType (type);
		int sign = -1;
		if (Random.Range (0, 100) < 50)
			sign = sign * -1;
		m_BallObject.GetComponent<Rigidbody>().AddForce (new Vector3(Random.Range (200,400) * sign, Random.Range (800,1000), 0));
		m_BallObject.GetComponent<Rigidbody> ().isKinematic = false;
		LeanTween.rotateAround( gameObject, Vector3.forward, 360f, 1f).setRepeat(-1);
	}
	public void UpdateBallParams(float ball_angle)
	{
		m_RigidBody.AddForce (new Vector3(ball_angle ,100,0));
		m_YVelocity += m_Step;
		m_YVelocity = Mathf.Clamp (m_YVelocity, -m_MaxYVelocity, m_MaxYVelocity);
	}
	public void SlowDown()
	{
		m_YVelocity = 5;
		Vector3 vel = m_RigidBody.velocity;
		vel.y = 5;
		vel.x *= 0.1f;
		m_RigidBody.velocity = vel;
	}
	void FixedUpdate ()
	{
		if (Mathf.Abs (m_RigidBody.velocity.y) > m_YVelocity) {
			Vector3 yvel = m_RigidBody.velocity;
			yvel.y = Mathf.Clamp (yvel.y, -m_YVelocity, m_YVelocity);
			m_RigidBody.velocity = yvel;
		} else if (Mathf.Abs (m_RigidBody.velocity.y) < m_YVelocity / 2) {
			Vector3 yvel = m_RigidBody.velocity;
			yvel.y += (yvel.y * 0.05f); 
			m_RigidBody.velocity = yvel;
		}
		ParticleSystem ps = m_TrailParticle.GetComponent<ParticleSystem> ();
		ps.startColor = Color.Lerp(Color.white, m_Color,  0.75f);
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.collider.CompareTag ("Wall")) {
			m_LastContactPoint = transform.position;
			m_GameController.PlayBallHitSfx ();
		} else if (other.collider.CompareTag ("ball")) {
			m_GameController.PlayBallHitSfx ();
			SetBallType(Random.Range(0,5));
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.CompareTag ("GameOver")) {
			PoolManager.ReleaseObject(this.gameObject);
			m_GameController.CheckEndConition();
		}
	}
}
