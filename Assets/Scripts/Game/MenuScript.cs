﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

	public bool isQuit;

	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnMouseUp()
	{
		if(isQuit)
			Application.Quit();
		else
		{

			switch(gameObject.name)
			{
			case "txt_main_menu":
				GameObject.Find ("Game Controller").GetComponent<GameController>().DisablePause();
				GetComponent<GUIText>().color = Color.white;
				Application.LoadLevel(0);
				break;
			case "txt_retry":
				Application.LoadLevel(1);
				break;
			case "txt_continue":
				GameObject.Find ("Game Controller").GetComponent<GameController>().DePause();
				break;
			case "txt_new_game":
				Application.LoadLevel(1);
				break;

			}

		}
	}

	void OnMouseEnter()
	{

		GetComponent<GUIText>().color = Color.green;

	}

	void OnMouseExit()
	{
		GetComponent<GUIText>().color = Color.white;

	}
}
